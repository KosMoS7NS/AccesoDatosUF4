package org.ifp.shared;

import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.lang.System.*;

public class Menu {

    public static String NOMBRE;
    public static String APELLIDOS;
    public static int EDAD;
    public static int NUMERO_TELEFONO;
    public static int HIERRO_ACTUAL;
    public static int HIERRO_MINIMO;
    public static int HIERRO_ACTUALIZADO;
    public static int RESPUESTA;

    /**
     * Menú para crear el paciente o salir dependiendo de la respuesta del usuario.
     * @return
     */
    public int menuPersona() {
        Scanner scanner = new Scanner(in);
        List.of("|==================|",
                "|1) Crear paciente |",
                "|0) Salir          |",
                "|==================|")
                .forEach(out::println);

        out.println("Introduce una opción: ");
        RESPUESTA = scanner.nextInt();

        switch (RESPUESTA) {
            case 1:
                crearPaciente(scanner);
                break;

            case 0:
                break;
        }

        return RESPUESTA;
    }

    /**
     * Método para crear un paciente con los datos proporcionados por el usuario
     * @param scanner
     */
    public static void crearPaciente(Scanner scanner) {
        out.println("Introduce el nombre de la persona: ");
        NOMBRE = scanner.next();

        scanner.nextLine();
        out.println("Introduce los apellidos de la persona: ");
        APELLIDOS = scanner.nextLine();

        out.println("Introduce la edad de la persona: ");
        EDAD = scanner.nextInt();

        out.println("Introduce el número de telefono de la persona: ");
        NUMERO_TELEFONO = scanner.nextInt();

        out.println("Introduce el hierro actual de la persona: ");
        HIERRO_ACTUAL = scanner.nextInt();

        out.println("Introduce el hierro mínimo de la persona: ");
        HIERRO_MINIMO = scanner.nextInt();

        out.println("Introduce el hierro actualizado de la persona: ");
        HIERRO_ACTUALIZADO = scanner.nextInt();
    }
}
