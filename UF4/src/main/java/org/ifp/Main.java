package org.ifp;

import org.ifp.analisis.Analisis;
import org.ifp.persona.Paciente;
import org.ifp.shared.Menu;

import static org.ifp.shared.Menu.*;

public class Main {
    /**
     * Método main en el cual se instancia el Paciente con datos proporcionados por el usuario y el Análisis.
     * Se instancia el menú para que el usuario pueda decidir que acción realizar.
     * Se realiza los sets correspondientes para instanciar el Paciente en el Análisis y los métodos de
     * addPropertyChangeListener() y setHierro_actual().
     *
     * @author Daniel Díez Miguel
     * @param args
     */
    public static void main(String[] args) {
        new Menu().menuPersona();
        Paciente paciente = new Paciente(NOMBRE, APELLIDOS, EDAD, NUMERO_TELEFONO, HIERRO_ACTUAL, HIERRO_MINIMO);
        Analisis analisis = new Analisis();

        analisis.setPaciente(paciente);
        paciente.addPropertyChangeListener(analisis);
        paciente.setHierro_actual(HIERRO_ACTUALIZADO);
    }
}