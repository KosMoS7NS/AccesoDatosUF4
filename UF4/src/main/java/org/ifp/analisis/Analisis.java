package org.ifp.analisis;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.ifp.persona.Paciente;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;

@Getter
@Setter
public class Analisis implements Serializable, PropertyChangeListener {
    private Paciente paciente;

    /**
     * Método propertyChangeEvent().
     * Muestra el análisis de una persona.
     * @param evt Describe las propiedades antiguas, y las que han sido modificadas.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        System.out.printf("%n*******************%n" +
                "     ANALISIS%n*******************" +
                "%nNOMBRE: %s" +
                "%nAPELLIDOS: %s" +
                "%nEDAD: %s" +
                "%nTELEFONO: %s" +
                "%nHIERRO ANTIGUO: %s %n" +
                "HIERRO ACTUAL: %s %n", paciente.getNombre(), paciente.getApellido(), paciente.getEdad(),
                paciente.getTelefono(), evt.getOldValue(), evt.getNewValue());
    }
}
