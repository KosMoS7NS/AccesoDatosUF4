package org.ifp.persona;

import lombok.Getter;
import lombok.Setter;
import org.ifp.shared.Menu;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.ifp.shared.Menu.HIERRO_ACTUAL;
import static org.ifp.shared.Menu.HIERRO_ACTUALIZADO;

@Getter
@Setter
public class Paciente implements Serializable {
    private String nombre;
    private String apellido;
    private int edad;
    private int telefono;
    private int hierro_actual;
    private int hierro_minimo;
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Constructor con argumentos del Paciente.
     *
     * @param nombre
     * @param apellido
     * @param edad
     * @param telefono
     * @param hierro_actual
     * @param hierro_minimo
     */
    public Paciente(String nombre, String apellido, int edad, int telefono, int hierro_actual, int hierro_minimo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.telefono = telefono;
        this.hierro_actual = hierro_actual;
        this.hierro_minimo = hierro_minimo;
    }

    /**
     * Si el hierro es mayor por 10 respecto al valor que tiene asignado el paciente, salta el evento.
     * Se genera nuevos datos de una Persona si la diferencia es superior a 10.
     *
     * @param new_value
     */
    public void setHierro_actual(int new_value) {
        Scanner scanner = new Scanner(System.in);
        int old_value = HIERRO_ACTUAL;
        HIERRO_ACTUAL = new_value;

        Function<Integer, Integer> resta = integer -> integer - getHierro_minimo();
        Predicate<Integer> check = integer -> integer > 10;

        if (check.test(resta.apply(new_value))) {
            propertyChangeSupport.firePropertyChange("hierro_actual", old_value, HIERRO_ACTUAL);
            System.out.println(
                    "\n*************************************************************************************************" +
                    "\nAl tener una variación de hierro mínimo superior a 10, se van a actualizar los datos del paciente" +
                    "\n*************************************************************************************************\n");
            new Menu().crearPaciente(scanner);
            setHierro_actual(HIERRO_ACTUALIZADO);
        }
    }

    /**
     * Método addPropertyChangeListener() en el cual sirve para añadir el evento.
     *
     * @param propertyChangeListener
     */
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
    }
}
